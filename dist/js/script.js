const hbm = document.querySelector(".header__menu");
const hb = document.querySelector(".header__burger");

hb.addEventListener("click", function (event) {
    hbm.classList.toggle("active-menu");
    hb.classList.toggle("active-burger");
    event.stopPropagation();
})

document.body.addEventListener("click", function (event) {
    if (event.target !== hbm) {
        if (hbm.classList.contains("active-menu")) {
            hbm.classList.remove("active-menu")
        }
        if (hb.classList.contains("active-burger")) {
            hb.classList.remove("active-burger")
        }
    }
})